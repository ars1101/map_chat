import 'package:chat/core/holder.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:chat/auth/data/repository/supawidgets.dart';
import 'package:chat/auth/prestation/pages/forgotpass.dart';
import 'package:chat/auth/prestation/pages/otp.dart';
import 'package:chat/auth/prestation/pages/signin.dart';
import 'package:chat/auth/prestation/pages/signup.dart';
import 'package:chat/auth/prestation/widgets/textfield.dart';
import 'package:chat/core/color.dart';

import 'package:supabase_flutter/supabase_flutter.dart';





class chat extends StatefulWidget {
  const chat({super.key});

  @override
  State<chat> createState() => _chatState();
}

List<Map<String, dynamic>> riders = [];
class _chatState extends State<chat> {

  Map<String,dynamic> d = {};
  bool rider = false;


  @override
  void initState() {
    super.initState();
    getUser().then((value) => {
      setState(() {
    rider = value['rider'];
      })
    });
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      riders = await getRiders(!rider);
      setState(() {

      });
    });
    print(riders);
  }

  int ind = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(itemBuilder: (_, index){
        return GestureDetector(child: Text(riders[index]['fullname']), onTap: (){
          createChat(riders[index], rider);

        },);
      }, itemCount: riders.length,)
    );
  }
}
