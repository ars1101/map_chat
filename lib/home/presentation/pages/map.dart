import 'package:chat/core/holder.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:chat/auth/data/repository/supawidgets.dart';
import 'package:chat/auth/prestation/pages/forgotpass.dart';
import 'package:chat/auth/prestation/pages/otp.dart';
import 'package:chat/auth/prestation/pages/signin.dart';
import 'package:chat/auth/prestation/pages/signup.dart';
import 'package:chat/auth/prestation/widgets/textfield.dart';
import 'package:chat/core/color.dart';

import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';





class map extends StatefulWidget {
  const map({super.key});

  @override
  State<map> createState() => _mapState();
}

List<Map<String, dynamic>> riders = [];
class _mapState extends State<map> {

  Map<String,dynamic> d = {};
  bool rider = false;
  Position? position;

  @override
  void initState() {
    super.initState();
    Geolocator.getLastKnownPosition().then((value) => {setState(() {
      position = value;
      print(value);
    })});
  }

  int ind = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: YandexMap(
        mapObjects: [PlacemarkMapObject(mapId: MapObjectId('map'), point: Point(latitude: position!.latitude, longitude: position!.longitude), icon: PlacemarkIcon.single(
          PlacemarkIconStyle(
            image: BitmapDescriptor.fromAssetImage(
              'assets/Pin.png',
            ),
            scale: 22,
          ),
        ))],
      ));
  }
}
