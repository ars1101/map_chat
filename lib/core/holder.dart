import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:chat/auth/data/repository/supawidgets.dart';
import 'package:chat/auth/prestation/pages/signin.dart';
import 'package:chat/auth/prestation/widgets/textfield.dart';
import 'package:chat/core/color.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class Holder extends StatefulWidget {
  const Holder({super.key});

  @override
  State<Holder> createState() => _HolderState();
}

User? user;

class _HolderState extends State<Holder> {
  double dispwidth = 0;
  Uri pdf = Uri.parse('https://lwtgrvmdvlhofehpmgtb.supabase.co/storage/v1/object/public/policy/rud%20(1).pdf?t=2024-02-13T19%3A55%3A19.306Z');
  TextEditingController name = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController pass = TextEditingController();
  TextEditingController passc = TextEditingController();
  bool obs = true;
  bool check = false;
  bool isValid() {
    if ((name.text.length > 0) &
    EmailValidator.validate(email.text) &
    (phone.text.length > 0) &
    (email.text.length > 0) &
    (pass.text.length >= 6) &
    check &
    (pass.text == passc.text)) {
      return true;
    } else {
      return false;
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Row(),
      ),
    );
  }
}
