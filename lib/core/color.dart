import 'package:flutter/material.dart';
import 'package:chat/main.dart';
import 'package:flutter/material.dart';

Color fblack = Color(0xFF3A3A3A);
Color dblue = Color(0xFF0560FA);
Color grey2 = Color(0xFFA7A7A7);

var LightTheme = ThemeData(colorScheme: ColorScheme.fromSeed(seedColor: Colors.white, background: Colors.white),
    backgroundColor: Colors.black,
    filledButtonTheme: FilledButtonThemeData(
        style: FilledButton.styleFrom(
            foregroundColor: Colors.white,
            backgroundColor: Color(0xFF0560FA),
            shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
            disabledForegroundColor: Colors.white,
            disabledBackgroundColor: Color(0xFFA7A7A7))),
    fontFamily: 'Roboto',
    textTheme: TextTheme(
        labelLarge: TextStyle(
            color: Colors.black,
            fontSize: 24,
            fontWeight: FontWeight.w500,
            fontFamily: 'Roboto'),
        titleSmall: TextStyle(
            color: Color(0xFFA7A7A7),
            fontSize: 12,
            fontWeight: FontWeight.w500,
            fontFamily: 'Roboto')),
    inputDecorationTheme: InputDecorationTheme( hintStyle: TextStyle(
        color: Color(0xFFA7A7A7),
        fontSize: 14,
        fontWeight: FontWeight.w500,
        fontFamily: 'Roboto') ,

        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(4),
            borderSide: BorderSide(color: Color(0xFFA7A7A7)))));

var DarkTheme = ThemeData(
    colorScheme: ColorScheme.fromSeed(seedColor: Colors.black, background: Colors.black),
    backgroundColor: Color(0xFF000D1D),
    filledButtonTheme: FilledButtonThemeData(
        style: FilledButton.styleFrom(
            foregroundColor: Color(0xFF000D1D),
            backgroundColor: Color(0xFF0560FA),
            shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
            disabledForegroundColor: Colors.white,
            disabledBackgroundColor: Color(0xFFA7A7A7))),
    fontFamily: 'Roboto',
    textTheme: TextTheme(
        labelLarge: TextStyle(
            color: Colors.black,
            fontSize: 24,
            fontWeight: FontWeight.w500,
            fontFamily: 'Roboto'),
        titleSmall: TextStyle(
            color: Color(0xFFA7A7A7),
            fontSize: 12,
            fontWeight: FontWeight.w500,
            fontFamily: 'Roboto')),
    inputDecorationTheme: InputDecorationTheme( hintStyle: TextStyle(
        color: Color(0xFFA7A7A7),
        fontSize: 14,
        fontWeight: FontWeight.w500,
        fontFamily: 'Roboto') ,

        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(4),
            borderSide: BorderSide(color: Color(0xFFA7A7A7)))));
