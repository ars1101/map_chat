import 'dart:math';

import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:chat/auth/data/repository/supawidgets.dart';
import 'package:chat/auth/prestation/pages/forgotpass.dart';
import 'package:chat/auth/prestation/pages/newpass.dart';
import 'package:chat/auth/prestation/pages/otp.dart';
import 'package:chat/auth/prestation/pages/signin.dart';
import 'package:chat/auth/prestation/pages/signup.dart';
import 'package:chat/auth/prestation/widgets/textfield.dart';
import 'package:chat/core/color.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:pinput/pinput.dart';

class Otp extends StatefulWidget {
  const Otp({super.key});

  @override
  State<Otp> createState() => _OtpState();
}

User? user;

class _OtpState extends State<Otp> {
  @override
  void initState() {}
  Uri pdf = Uri.parse(
      'https://lwtgrvmdvlhofehpmgtb.supabase.co/storage/v1/object/public/policy/rud%20(1).pdf?t=2024-02-13T19%3A55%3A19.306Z');
  TextEditingController pin = TextEditingController();
  bool obs = true;
  bool check = false;

  bool isValid() {
    if ((pin.text.length == 6)) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: 24,
            ),
            Column(
              children: [
                SizedBox(
                  height: 78,
                ),
                Text(
                  'OTP Verification',
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                SizedBox(
                  height: 8,
                ),
                Text(
                  'Enter the 6 digit numbers sent to your email',
                  style: Theme.of(context).textTheme.titleSmall,
                ),
                SizedBox(
                  height: 52,
                ),
                SizedBox(
                    height: 32,
                    width: MediaQuery.of(context).size.width - 48,
                    child: Pinput( length: 6,
                      defaultPinTheme: PinTheme(
                        width: 32,
                        height: 32,
                        decoration:
                            BoxDecoration( border: Border.all(color: grey2),
                                borderRadius: BorderRadius.zero),
                      ),
                      separatorBuilder: (context) => SizedBox(
                        width: 30,
                      ),
                      submittedPinTheme: PinTheme(
                        width: 32,
                        height: 32,
                        decoration: BoxDecoration( border: Border.all(color: dblue),
                          borderRadius: BorderRadius.zero,
                        ),
                      ),
                      controller: pin,
                    )),
                SizedBox(
                  height: 24,
                ),
                SizedBox(
                  height: 56,
                ),
                SizedBox(
                  height: 46,
                  width: MediaQuery.of(context).size.width - 48,
                  child: FilledButton(
                    onPressed: (isValid() != true)
                        ? null
                        : () async {},
                    child: Text('Set New Password'),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width - 48,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Remember password? Back to',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: Color(0xFFA7A7A7)),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => SignIn()));
                        },
                        child: Text(
                          'Sign in',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              color: Color(0xFF0560FA)),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 28,
                )
              ],
              crossAxisAlignment: CrossAxisAlignment.start,
            ),
            SizedBox(
              width: 24,
            )
          ],
        ),
      ),
    );
  }
}
